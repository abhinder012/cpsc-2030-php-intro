<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PHP Intro Lab</title>
  </head>
  <body class="container">
    <h1>PHP Intro Lab</h1>

    <form action="">
        <div class="form-group">
          <label for="word">Word</label>
          <input type="text" name="word" id="word" class="form-control" placeholder="enter word to hightlight">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Highlight</button>
        </div>
    </form>
    
    <?php
        ini_set('display_errors',1);
        $sonnet = "This point has been seen in almost all discoveries. Observing previous theories for scientists is like a learning new things for  a 4-year-old boy. He learns new things from his elders, who have experience or already discover things in their young age. Sometimes, he learns/ discover things by himself like learning how to eat, speak, play and so on. Also, he makes his own ways by refining their elders like how to study. Moving towards in science, scientists observe previous knowledge, learn previous methods and tries to refine them for a perfection of that specific theory. They usually focus on predictions to reach out the results.
As we know about Kepler’s Planetary Motion Laws, discovered three laws in between 1609-1619, which defined about motion of planets around sun. He analyzes Nicolaus Copernicus heliocentric theory, refined it and discovered first two laws in 1609 and third laws discovered in 1619.After two decades, Kepler’s laws were useful for Godefroy Wendelin. Moving towards other example, Gravitational Waves, which is the disturbance in spacetime proposed by Henri Pioncare and predicted by Einstein in late 20 century. By the observation of their prediction in 2016 scientists were able to make a device which can measure gravitational waves.
These two theories make it clear that, scientists observes previous knowledge, either to refine them or discover new theories, which helps to understands science more deeply.
";

        $wordCount = 0;
        
        $word = "";
        if ( isset( $_GET["word"]) ) {
            $word = trim( $_GET["word"]);
            
            $wordCount = substr_count( $sonnet, $word );

            $sonnet = str_replace( $word, "<span style='background-color: #88ff88'>" . $word . "</span>", $sonnet );            
            
            $sonnet = nl2br( $sonnet );
        }
    ?>
    <h2>
        <?php print $word ?> appears <?php print $wordCount ?> times in this sonnet.
    </h2>
    <div>
        <?php
            // print the sonnet with highlighting
            print $sonnet;
        ?>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
  </body>
</html>